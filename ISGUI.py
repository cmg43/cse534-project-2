import tkinter
from tkinter import filedialog
import cv2
import os
import PIL.Image, PIL.ImageTk
from numpy import *
from imageSearcher import *

#https://solarianprogrammer.com/2018/04/20/python-opencv-show-image-tkinter-window/
#how to display open_cv images in a gui taken from here

#uses of open cv refrenced from
#https://docs.opencv.org

class Window:

    def __init__(self):
        self.curImage = None
        #window
        self.top = tkinter.Tk()
        #search button
        self.SB = tkinter.Button(self.top, text = "search", command = self.searchImage)
        #browse for query image
        self.BB = tkinter.Button(self.top, text = "browse", command = self.askopenfile)
        #previous image in result
        self.PB = tkinter.Button(self.top, text = "previous", command = self.nextimage)
        #next image in result
        self.NB = tkinter.Button(self.top, text = "next", command = self.previmage)
        self.BB.grid(row=0, column=0)
        self.SB.grid(row=0, column=1)
        self.PB.grid(row=1, column=0)
        self.NB.grid(row=1, column=1)
        self.top.mainloop()

    
    def searchImage(self):
        #set current result to display to none
        self.curImage = None
        print("searching...")
        #get results from the image searcher
        self.results = imageSearcher(self.filename)
        #save results to file
        f = open(self.output,"w+")
        f.write(str(self.results))
        f.close()
        #show the first image of results
        self.showimage(0)
    def askopenfile(self):
        #open file defaults to jpg files
        filetypes = (
            ("JPEG files", "*.jpg"),
            ("Portable Network Graphics", "*.png"),
            ("All Files", "*.*")
        )
        #get file 
        self.file =  filedialog.askopenfile(mode='rb',title='Choose a file', filetypes = filetypes)
        if self.file.name != None:
            #if successfully has new file
            self.output = +str(os.path.basename(self.file.name))+"_result.txt"
            self.filename = str(self.file.name)
            #create open cv image from file
            cv_img = cv2.cvtColor(cv2.imread(self.filename), cv2.COLOR_BGR2RGB)
            height, width, no_channels = cv_img.shape
            #create image space of left and right to same size as query image
            self.lcanvas = tkinter.Canvas(self.top, width = width, height = height)
            self.rcanvas = tkinter.Canvas(self.top, width = width, height = height)
            self.lcanvas.grid(row=2, column=0)
            self.rcanvas.grid(row=2, column=1)
         
            # Use PIL (Pillow) to convert the NumPy ndarray to a PhotoImage
            self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(cv_img))
         
            # Add a PhotoImage to the Canvas
            self.lcanvas.create_image(0, 0, image=self.photo, anchor=tkinter.NW)
        
    def nextimage(self):
        #change current image to next image
        self.showimage(1)
    def previmage(self):
        #change current image to previous image
        self.showimage(-1)
    def showimage(self,increment):
        print(len(self.results))
        #if new search set image to top result last in results array
        if self.curImage == None:
            self.curImage = len(self.results)-1
        #else if image is at 0 do not move image below 0
        elif self.curImage <= 0 and increment < 0:
            self.curImage = 0
        #else if image is at max array length stay at max array length
        elif self.curImage == len(self.results)-1:
            self.curImage += 0
        #else increment based on next or previous
        else:
            self.curImage += increment
        print(self.curImage,self.results[self.curImage])
        cv_img = cv2.cvtColor(cv2.imread(self.results[self.curImage][0]), cv2.COLOR_BGR2RGB)
        # Use PIL (Pillow) to convert the NumPy ndarray to a PhotoImage
        self.photor = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(cv_img))
         
        # Add a PhotoImage to the Canvas
        self.rcanvas.create_image(0, 0, image=self.photor, anchor=tkinter.NW)
        #print(increment)
        
def main():
    #create window
    win = Window()
main()
