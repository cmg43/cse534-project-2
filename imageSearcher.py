import cv2
import os
from operator import itemgetter
from imageDescriptor import imageDescriptor
import time

def imageSearcher(queryImage):
    #empty list of results
    results = []
    #base path for the images to be searched
    path = 'imgdb\paris\\'
    #create a brute force matcher
    bf = cv2.BFMatcher()
    obf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    #set the queryImage given by user
    queryImage = imageDescriptor(queryImage)
    #check start for timing purposes
    start = time.time()
    count = 0
    #search each file in each directory within the path
    for r, d, f in os.walk(path):
        for file in f:
            #try catch to ignore any corrupted image files if corrupted print filename
            try:
                
                #create and imageDescriptor object for each image file in search
                image = imageDescriptor(os.path.join(r,file))
                #check matches between query image and current image
                #matches = bf.knnMatch(queryImage.getFeatures()[1],image.getFeatures()[1], k=2)
                omatches = obf.match(queryImage.getFeatures()[1],image.getFeatures()[1])
                omatches = sorted(omatches, key = lambda x:x.distance)
                #create empty list of good images
                good = []
                for m in omatches:
                    if m.distance < 50:
                        good.append(m)
                    else:
                        break
                #print(len(omatches))
                #print(len(good))
                
                #print("next:")
                #determine good matches based on their distance
                #for m,n in matches:
                    #if m.distance < 0.75*n.distance:
                        #good.append([m])
                #print(len(good))
                #if the number of good matches is above 5 include in the results
                if len(good) > 10:
                    match = [os.path.join(r,file),len(good)]
                    results.append(match)
                    #keep results sorted based on the # of good matches
                    results = sorted(results, key=itemgetter(1))
                
            except:
                print(os.path.join(r,file))
            
                    
    #get end time
    end = time.time()
    #print search time
    print("done:", end-start)
    #return list of sorted match results
    return results
    
