from cv2 import *

class imageDescriptor:

    def __init__(self, image):
        self.features = []
        #read in the image
        self.originalimg = cv2.imread(image,1)
        #convert image to HSV
        self.img = cv2.cvtColor(self.originalimg, cv2.COLOR_BGR2HSV)
        #create the sift feature detector
        #sift = cv2.xfeatures2d.SIFT_create()
        #get the features
        #keypoints, descriptors = sift.detectAndCompute(self.img,None)
        orb = cv2.ORB_create()
        keypoints, descriptors = orb.detectAndCompute(self.img,None)

        
        #add kepoints and descriptors to the features
        self.features.append(keypoints)
        self.features.append(descriptors)

    #return the HSV image
    def getImg(self):
        return self.img

    #return the features list
    def getFeatures(self):
        return self.features

    #displav the original and HSV converted image using open cv for debug purposes
    def showImg(self):
        cv2.imshow("HSV",self.img)
        cv2.imshow("original", self.originalimg)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

